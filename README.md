# scriptable-nightscout-widget

![widget](widget.jpeg)

Widgetfor scriptable.app to display nightscout data on an iOS device.

# update
- 27.10.2020 - readded update time
- 27.10.2020 - add option to show glucose in mmol/l
- 26.10.2020 - switches to nightscout v2 api

# install
- download https://scriptable.app 
- create a new script
- copy nightscout.js content to the new script
- replace ```nsUrl``` and ```nsToken``` with your data
- set ```glucosDisplay``` to `mmoll` to show glucose in mmol/l
- change ```dateFormat```  to `de-DE` to show update time in german
- add widget to you iOS device


